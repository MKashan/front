import { TestBed, inject } from '@angular/core/testing';

import { GlobalServService } from './global-serv.service';

describe('GlobalServService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GlobalServService]
    });
  });

  it('should ...', inject([GlobalServService], (service: GlobalServService) => {
    expect(service).toBeTruthy();
  }));
});
