import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-add-result',
  templateUrl: './add-result.component.html',
  styleUrls: ['./add-result.component.css']
})
export class AddResultComponent implements OnInit {
  data = new Data();
  id: string;
  toggle= false;
  message:string;
  year:string;
  stud:number;
  student=[];
  cour:number;
  course=[];
  constructor(private  authan: GlobalServService) { }

  ngOnInit() {
    this.year='2009';
    this.authan.getStudents().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.stud=responce.data[0].id;
              this.student= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.cour=responce.data[0].id;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.year= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.stud= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.cour= deviceValue.target.value;

  }
  toggles(){
    this.toggle = false;
  }
  add() {
this.data.year=this.year;
this.data.resCourseId=this.cour;
this.data.resStudentId=this.stud;
console.log('sdsadsadsadsadsad',this.stud)
    this.authan.addResult(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message;

              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }



}
class Data{

  marks:number;
  grade:string;
  type:string;
  year:string;
  resStudentId;
  resCourseId;
}
