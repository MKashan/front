import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseInSectionComponent } from './course-in-section.component';

describe('CourseInSectionComponent', () => {
  let component: CourseInSectionComponent;
  let fixture: ComponentFixture<CourseInSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseInSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseInSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
