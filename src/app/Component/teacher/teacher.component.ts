import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
  veiw=true;
  del=true;
  add=true;
  upd=true;
  tcamp=true;
  tcourse=true;
  constructor() { }

  ngOnInit() {
    if(localStorage.getItem('type') == 'loginStudent'){

      this.del=false;
      this.upd=false;
      this.tcamp=false;
      this.tcourse=false;
    }
  }

}
