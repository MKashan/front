import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
import {document} from '@angular/platform-browser/src/facade/browser';
@Component({
  selector: 'app-add-admin',
  templateUrl: './add-admin.component.html',
  styleUrls: ['./add-admin.component.css']
})
export class AddAdminComponent implements OnInit {

  type: string;//used to store the gender of admin
  data= {gender:''};//used to store data at front end and forward to database
  toggle= false;//used to hide and show the message bar
  message:string;//used to store message which come from database
  constructor(private  authan: GlobalServService) { }

  ngOnInit() {
    this.type = document.getElementById('selectAccType').value;
  }
  toggles(){
    this.toggle= false;
  }
  on(deviceValue) {
    this.type= deviceValue.target.value;

  }
  addAdmin() {

    this.data.gender=this.type;
    this.authan.addAdmin(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;

              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }

}
