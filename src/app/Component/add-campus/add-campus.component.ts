import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-add-campus',
  templateUrl: './add-campus.component.html',
  styleUrls: ['./add-campus.component.css']
})
export class AddCampusComponent implements OnInit {
     data = new Datas()//used to store data at front end and forward to database
     id: string;
  toggle= false;//used to hide and show the message bar
  message:string;//used to store message which come from database
  constructor(private  authan: GlobalServService) { }

  ngOnInit() {
  }
  toggles(){
    this.toggle = false;
  }
     addCampus() {

    this.authan.addCampuses(this.data).subscribe(
      responce => {

        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message;

              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }





}
class Datas {
  name: string;
  location: string;
  address: string;
  adminId: number;
}
