import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {
   view=true;
  add=true;
  del=true;
  upd=true;
  staffToCampus=true;


  constructor() { }

  ngOnInit() {
    if(localStorage.getItem('type') == 'loginStaff'){
      this.add=false;
      this.del=false;
      this.upd=false;
    }

  }

}
