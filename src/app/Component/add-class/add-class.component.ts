import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-add-class',
  templateUrl: './add-class.component.html',
  styleUrls: ['./add-class.component.css']
})
export class AddClassComponent implements OnInit {
  data=new Data();//used to store data at front end and forward to database
  id: string;
  toggle= false;//used to hide and show the message bar
  message:string;//used to store message which come from database
  campus=[];//used to store Array of Campuses which come from database
  teacher=[];//used to store Array of teacher which come from database
  section=[];//used to store Array of sections which come from database
  clas:number;
  teach:number;
  camp:number;
  secName:string;
  constructor(private  authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('sdsadsadsadasdsad',responce.data)
          switch ( responce.status) {
            case 200:
              this.camp=responce.data[0].id;
              this.campus= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getteachers().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.teach=responce.data[0].id;
              this.teacher= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:

              this.section= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  toggles(){
    this.toggle = false;
  }
  on(deviceValue) {
    this.camp= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.teach= deviceValue.target.value;

  }


  add() {
  this.data.sectionName=this.secName;
  this.data.campusId=this.camp;
  this.data.teacherId=this.teach;
  this.data.class=this.clas;
    this.authan.addSection(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message;

              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }

}
class Data {
sectionName:string;
class:number;
teacherId:number;
campusId:number;

}
