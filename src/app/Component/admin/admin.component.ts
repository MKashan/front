import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
   veiw= true;
  add= false;
 upd= false;
 del= false;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    if ((this.authan.userdetails.id == 1) && (localStorage.getItem('type') == 'loginAdmin')){
        this.add = true;
        this.upd = true;
        this.del = true;
    }

  }

}
