import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
import {Http} from '@angular/http';
import {Router} from '@angular/router';
import {document} from '@angular/platform-browser/src/facade/browser';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
responce = { };
userId: string;
password: string;
type: string;
  toggle = false;
  message: string;
  constructor(private http: Http, private router: Router, private authan: GlobalServService) {


  }

  ngOnInit() {
    this.type = document.getElementById('selectAccType').value;
  }
  toggles(){
    this.toggle= false;
  }
  on(deviceValue) {
    this.type= deviceValue.target.value;
  }
  login(): void {
    localStorage.setItem('type',this.type);
   // this.router.navigate((['home']));
    this.authan.login(this.userId, this.password, this.type).subscribe(

      responce => {
        console.log('sdsd', responce.data);
        if (responce.status != null) {

          switch (responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              this.authan.userdetails = responce.data;
              if(localStorage.getItem('type')=='loginStudent'){
                this.authan.studendtDetails=responce.data;
              }
              if(localStorage.getItem('type') == 'loginTeacher'){
                this.authan.teacherDetails=responce.data;
              }
              if(localStorage.getItem('type') == 'loginStaff'||localStorage.getItem('type') == 'loginAdmin'){
                this.authan.staffOrAdminDetails=responce.data;
              }
              localStorage.setItem('Data', responce.data.token);
              localStorage.setItem('token', responce.data.token);
             this.router.navigate((['home']));
            //  localStorage.removeItem('token');
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;
          }


        }
      }
    );


  }

}
