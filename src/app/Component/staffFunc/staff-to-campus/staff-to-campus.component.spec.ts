import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffToCampusComponent } from './staff-to-campus.component';

describe('StaffToCampusComponent', () => {
  let component: StaffToCampusComponent;
  let fixture: ComponentFixture<StaffToCampusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffToCampusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffToCampusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
