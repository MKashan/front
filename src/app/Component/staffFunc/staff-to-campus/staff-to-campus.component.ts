import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';

@Component({
  selector: 'app-staff-to-campus',
  templateUrl: './staff-to-campus.component.html',
  styleUrls: ['./staff-to-campus.component.css']
})
export class StaffToCampusComponent implements OnInit {
  campusId: string;
  staffId: string;
  value: string;
  responce= {};
  data= new Data();
  toggle= false;
  message: string;
  staff=[];
  campus=[];
  constructor(private authan: GlobalServService) { }
  ngOnInit() {
    this.toggle= false;
    this.authan.getStaffs().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.staff= responce.data;
              this.data.staffId=responce.data[0].id;
              this.staffId=responce.data[0].id;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.campus= responce.data;
              this.data.campId=responce.data[0].id;
              this.campusId=responce.data[0].id;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );


  }
  on(deviceValue) {
    this.data.campId= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.data.staffId= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.campusId= deviceValue.target.value;

  }
  on4(deviceValue) {
    this.staffId= deviceValue.target.value;

  }
  add(){
    this.authan.addStaffInCamp(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;

              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;

              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }

  delete(){
    console.log('camp',this.campusId)
    console.log('staff',this.staffId)
    this.authan.deletStaffCamp(this.campusId,this.staffId).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              console.log('sdsdsd', responce.message);
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );

  }


}
class Data{
  campId:number;
  staffId;number;
}
