import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit {
  view= true;
  add= true;
  delete= true;
  edit= true;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    if(localStorage.getItem('type') == 'loginStudent'||localStorage.getItem('type') == 'loginTeacher'){
      this.add = false;
      this.delete = false;
      this.edit = false;
    }
  }

}
