import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
import {document} from '@angular/platform-browser/src/facade/browser';
@Component({
  selector: 'app-add-staff',
  templateUrl: './add-staff.component.html',
  styleUrls: ['./add-staff.component.css']
})
export class AddStaffComponent implements OnInit {

  type: string;
  data= new Data();
  toggle= false;
  message:string;
  constructor(private  authan: GlobalServService) { }

  ngOnInit() {
    this.type = document.getElementById('selectAccType').value;
  }
  toggles(){
    this.toggle= false;
  }
  on(deviceValue) {
    this.type= deviceValue.target.value;

  }
  addStaff() {

    this.data.gender=this.type;
    this.authan.addStaff(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;

              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }

}
class Data{
  firstName:string;
  lastName:string;
  age:number;
  address:string;
  email:string;
  password:string;
  gender:string;
  salary:string;

}
