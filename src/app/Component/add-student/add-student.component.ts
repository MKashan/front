import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
import {document} from '@angular/platform-browser/src/facade/browser';
@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {
  type: string;
  data= new Data();
  toggle= false;
  message:string;
  sectionName:string;
  class:number;
  constructor(private  authan: GlobalServService) { }

  ngOnInit() {
    this.type = document.getElementById('selectAccType').value;
  }
  on(deviceValue) {
    this.type= deviceValue.target.value;

  }
  toggles(){
    this.toggle= false;
  }
  addStudent(){

    this.data.gender=this.type;
    this.authan.addStud(this.data,this.sectionName,this.class).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;

              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }

}
class Data{
  firstName:string;
  lastName:string;
  age:number;
  contact:number;
  address:string;
  email:string;
  password:string;
  gender:string;


}
