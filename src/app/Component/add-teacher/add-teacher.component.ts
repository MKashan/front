import { Component, OnInit } from '@angular/core';
import {document} from '@angular/platform-browser/src/facade/browser';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.css']
})
export class AddTeacherComponent implements OnInit {
  type: string;
  data= new Data();
  toggle= false;
  message:string;
  constructor(private  authan: GlobalServService) { }

  ngOnInit() {
    this.type = document.getElementById('selectAccType').value;
  }
  on(deviceValue) {
    this.type= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  addTeacher(){

    this.data.gender=this.type;
    this.authan.addTeacher(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
}
}

class Data{
 firstName:string;
 lastName:string;
 age:number;
 contact:number;
 address:string;
 email:string;
 password:string;
 gender:string;
 salary:string;
 qualification:string;
  dateOfJoin:string;

}
