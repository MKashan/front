import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
import {document} from '@angular/platform-browser/src/facade/browser';
@Component({
  selector: 'app-add-fee',
  templateUrl: './add-fee.component.html',
  styleUrls: ['./add-fee.component.css']
})
export class AddFeeComponent implements OnInit {
  data = {dueDate : '' ,feesStudentId:0};
  id: string;
  toggle= false;
  message:string;
  Day:string;
  month:string;
  year:string;
  stud:number;
  student=[];
  constructor(private  authan: GlobalServService) { }
  ngOnInit() {
    this.Day = document.getElementById('selectAccType').value;
    this.month = document.getElementById('selectAccType2').value;
    this.year = document.getElementById('selectAccType3').value;
    this.authan.getStudents().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.stud=responce.data[0].id;
              this.student= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );

  }

  on(deviceValue) {
    this.Day= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.month= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.year= deviceValue.target.value;

  }
  on4(deviceValue) {
    this.stud= deviceValue.target.value;

  }

  toggles(){
    this.toggle = false;
  }
  add() {
      this.data.feesStudentId=this.stud;
       this.data.dueDate=this.Day+"/"+this.month+'/'+this.year;
    this.authan.addFees(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message;

              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }




}
