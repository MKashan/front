import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-del-exame',
  templateUrl: './del-exame.component.html',
  styleUrls: ['./del-exame.component.css']
})
export class DelExameComponent implements OnInit {
  id: string;
  responce= {};
  toggle= false;
  message: string;
  secId: string;
  courseId: string;
  course=[];
  section=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.toggle = false;
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.courseId=responce.data[0].id;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.secId=responce.data[0].id;
              this.section= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );

  }
  on(deviceValue) {
    this.secId = deviceValue.target.value;

  }
  on2(deviceValue) {
    this.courseId = deviceValue.target.value;

  }
  delete(){

    this.authan.deleteExam(this.secId,this.courseId).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }


}
