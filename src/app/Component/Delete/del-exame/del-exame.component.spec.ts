import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelExameComponent } from './del-exame.component';

describe('DelExameComponent', () => {
  let component: DelExameComponent;
  let fixture: ComponentFixture<DelExameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelExameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelExameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
