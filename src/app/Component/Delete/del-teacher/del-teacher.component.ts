import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-del-teacher',
  templateUrl: './del-teacher.component.html',
  styleUrls: ['./del-teacher.component.css']
})
export class DelTeacherComponent implements OnInit {
  value: string;
  responce= {};
  Datas= [];
  toggle= false;
  message: string;
  teacher=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getteachers().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.value=responce.data[0].id;
              this.teacher= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  toggles() {
    this.toggle = false;
  }
  on(deviceValue) {
    this.value= deviceValue.target.value;

  }
  delete(){
        console.log('sdasdsadas',this.value)
    this.authan.deleteTeacher(this.value).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message= responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

}
