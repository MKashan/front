import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelCourseComponent } from './del-course.component';

describe('DelCourseComponent', () => {
  let component: DelCourseComponent;
  let fixture: ComponentFixture<DelCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
