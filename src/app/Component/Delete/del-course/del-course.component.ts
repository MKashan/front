import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';

@Component({
  selector: 'app-del-course',
  templateUrl: './del-course.component.html',
  styleUrls: ['./del-course.component.css']
})
export class DelCourseComponent implements OnInit {
  name: string;
  class:string;
  responce= {};
  toggle= false;
  message: string;
  course=[];
  constructor(private authan: GlobalServService ){}
  ngOnInit() {
    this.toggle = false;
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.name=responce.data[0].name;
              this.class=responce.data[0].class;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on2(deviceValue) {
    this.class= deviceValue.target.value;

  }
  on(deviceValue) {
    this.name= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  delete(){

    this.authan.deleteCourse(this.name,this.class).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message= responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

}
