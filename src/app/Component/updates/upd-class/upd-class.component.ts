import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-upd-class',
  templateUrl: './upd-class.component.html',
  styleUrls: ['./upd-class.component.css']
})
export class UpdClassComponent implements OnInit {
  toggle= false;
  message: string;
  data= new Datas();
  name: string;
  Signal: boolean;
  updateToggle= false;
  campName:string;
  sectionName:string;
  class:number;
  campId:number;
  teach:number;
 ////
  sec:string;
  section=[];
  camp:number;
  campus=[];
  clas:number;
  teacher=[];
  temp1:number;
  temp2:number;
  temp3:string;
  temp4:number
  constructor(private authan: GlobalServService){ }
  ngOnInit() {
    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.sec=responce.data[0].name;
              this.sectionName=responce.data[0].name;
              this.class=responce.data[0].class;
              this.clas=responce.data[0].class;
              this.section= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.camp=responce.data[0].id;
              this.campId=responce.data[0].id;
              this.campus= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );

    this.authan.getteachers().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.teach=responce.data[0].id;
              this.teacher= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );

  }
  on(deviceValue) {
    this.camp= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.sec= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.clas= deviceValue.target.value;

  }
  on4(deviceValue) {
    this.campId= deviceValue.target.value;

  }
  on5(deviceValue) {
    this.sectionName= deviceValue.target.value;

  }
  on6(deviceValue) {
    this.class= deviceValue.target.value;

  }
  on7(deviceValue) {
    this.teach= deviceValue.target.value;

  }
  toggles() {
    this.toggle = true;
  }

  getData(){

    this.authan.getSection(this.camp,this.sec,this.clas).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('dsd',responce.data);
          switch ( responce.status) {
            case 200:
              this.Signal=true;
              this.updateToggle=true;
              this.toggle = true;
              this.temp1=responce.data[0].class;
              this.temp2=responce.data[0].secCampusId;
              this.temp3=responce.data[0].name;
              this.temp4=responce.data[0].secTeacherId;
              this.data.name=responce.data[0].name;
              this.data.class=responce.data[0].class;
              this.data.secTeacherId=responce.data[0].secTeacherId;
              this.data.secCampusId=responce.data[0].secCampusId;
              this.message= responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    if (this.Signal == true) {
      this.data.class=this.class;
      this.data.name=this.sectionName;
      this.data.secCampusId=this.campId;
      this.data.secTeacherId=this.teach;
      this.authan.updateSection(this.data,this.camp,this.sec, this.clas).subscribe(
        resp => {

          if (resp.status != null) {
            switch (resp.status) {
              case 201:
                this.updateToggle=false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }

  }


}
class Datas {
  name: string;
  class: number;
  secCampusId: number;
  secTeacherId: number;
}
