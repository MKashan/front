import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdFeeComponent } from './upd-fee.component';

describe('UpdFeeComponent', () => {
  let component: UpdFeeComponent;
  let fixture: ComponentFixture<UpdFeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdFeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdFeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
