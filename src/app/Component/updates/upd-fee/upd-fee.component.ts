import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-upd-fee',
  templateUrl: './upd-fee.component.html',
  styleUrls: ['./upd-fee.component.css']
})
export class UpdFeeComponent implements OnInit {

temp:string;
  studId: string;
  studId2:string;
  type: string;
  detail: string;
  toggle= false;
  message: string;
  data=new Datas();
  name: string;
  day: string;
  month: string;
  Date:string [];
  year: string;
  Signal:boolean;
  updateToggle=false;
  student=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getStudents().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.studId=responce.data[0].id;
              this.student= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }

  toggles() {
    this.toggle = true;
  }
  on(deviceValue) {
    this.day= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.month= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.year= deviceValue.target.value;

  }
  on4(deviceValue) {
    this.studId= deviceValue.target.value;

  }
  on5(deviceValue) {
    this.studId2= deviceValue.target.value;

  }
  getData(){
    this.authan.getFees(this.studId,this.type,this.detail).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('dsd',responce.data);
          switch ( responce.status) {
            case 200:
              this.Signal=true;
              this.updateToggle=true;
              this.toggle = true;
              this.temp=responce.data[0].feesStudentId;
              this.studId2=responce.data[0].feesStudentId;
              this.data.type=responce.data[0].type;
              this.data.detail=responce.data[0].detail;
              this.data.studentId=responce.data[0].feesStudentId;
              this.Date = responce.data[0].dueDate.split('/');
              this.day =this.Date[0];
              this.month=this.Date[1];
              this.year=this.Date[2];
              this.message= responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    if (this.Signal == true) {
      this.data.studentId=this.studId2;
      this.data.dueDate=this.day+'/'+this.month+'/'+this.year;
      this.authan.updateFees(this.data, this.studId,this.type,this.detail).subscribe(
        resp => {

          if (resp.status != null) {
            switch (resp.status) {
              case 201:
                this.updateToggle=false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }

  }

}
class Datas {
  type: string;
  detail: string;
  dueDate: string;
  studentId: string;
}
