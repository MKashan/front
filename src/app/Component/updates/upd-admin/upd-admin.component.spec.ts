import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdAdminComponent } from './upd-admin.component';

describe('UpdAdminComponent', () => {
  let component: UpdAdminComponent;
  let fixture: ComponentFixture<UpdAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
