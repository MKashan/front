import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
import {document} from '@angular/platform-browser/src/facade/browser';
@Component({
  selector: 'app-upd-exam',
  templateUrl: './upd-exam.component.html',
  styleUrls: ['./upd-exam.component.css']
})
export class UpdExamComponent implements OnInit {
  toggle= false;
  message: string;
  data = new Datas();
  name: string;
  courseId: string;
  class: string;
  Signal: boolean;
  updateToggle = false;
  Date: string [];
  day: string;
  mouth: string;
  year: string;
  section=[];
  course=[];
  temp1:number;
  temp2:number;
  constructor(private authan: GlobalServService) { }



  ngOnInit() {
    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.name=responce.data[0].id;
              this.section= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.courseId=responce.data[0].id;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  toggles() {
    this.toggle = true;
  }
  on(deviceValue) {
    this.day = deviceValue.target.value;

  }
  on2(deviceValue) {
    this.mouth = deviceValue.target.value;

  }
  on3(deviceValue) {
    this.year = deviceValue.target.value;

  }
  on4(deviceValue) {
    this.name = deviceValue.target.value;

  }
  on5(deviceValue) {
    this.courseId = deviceValue.target.value;

  }
  on6(deviceValue) {
    this.temp1 = deviceValue.target.value;

  }
  on7(deviceValue) {
    this.temp2 = deviceValue.target.value;

  }

  getData() {
    this.authan.getExams(this.name, this.courseId).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('osamssssa', responce.data);
          switch ( responce.status) {
            case 200:
              this.Signal = true;
              this.updateToggle = true;
              this.toggle = true;
              this.temp2=responce.data[0].exSectionId;
              this.temp1=responce.data[0].exCourseId;
              this.Date = responce.data[0].date.split('/');
             this.day = this.Date[0];
              this.mouth = this.Date[1];
              this.year = this.Date[2];
              this.data.exSectionId = responce.data[0].exSectionId;
              this.data.exCourseId = responce.data[0].exCourseId;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    this.data.date=this.day+'/'+this.mouth+'/'+this.year;
    this.data.exCourseId=this.temp1;
    this.data.exSectionId=this.temp2;
    if (this.Signal == true) {
      this.authan.updateExam(this.data,this.courseId,this.name).subscribe(
        resp => {
          console.log('update',this.data)
          if (resp.status != null) {
            switch (resp.status) {
              case 201:
                this.updateToggle=false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }

  }
}

class Datas {
  exSectionId: number;
  date: string;
  exCourseId: number;
}

