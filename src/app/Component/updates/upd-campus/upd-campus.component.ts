import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-upd-campus',
  templateUrl: './upd-campus.component.html',
  styleUrls: ['./upd-campus.component.css']
})
export class UpdCampusComponent implements OnInit {
  toggle= false;
  message: string;
  data=new Datas();
  name: string;
  Signal:boolean;
  updateToggle=false;
  campus=[];
  admin=[];
  value2:string;
  temp:string;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getAdmins().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.admin= responce.data;
              this.value2=responce.data[0].id;

              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );


    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.name=responce.data[0].name;
              this.campus= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );

  }

  toggles(){
    this.toggle = true;
  }

  on2(deviceValue) {
    this.value2= deviceValue.target.value;

  }
  on(deviceValue) {
    this.name= deviceValue.target.value;

  }
  getData(){

    this.authan.getCampus(this.name).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('dsd',responce.data);
          switch ( responce.status) {
            case 200:
              this.Signal=true;
              this.updateToggle=true;
              this.toggle = true;
              this.name= responce.data[0].id;
              this.temp=responce.data[0].adminId;
              this.data.name = responce.data[0].name;
              this.data.address= responce.data[0].address;
              this.data.location= responce.data[0].location;
              this.data.adminId= responce.data[0].adminId;
              this.message= responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    this.data.adminId=this.value2;
    console.log('datassssssssss',this.name);
    if (this.Signal == true) {
      this.authan.updateCampus(this.data, this.name).subscribe(
        resp => {

          if (resp.status != null) {
            switch (resp.status) {
              case 201:
                this.updateToggle=false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }

  }

}
class Datas {
  name: string;
  location: string;
  address: string;
  adminId: string;
}
