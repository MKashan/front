import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-admin-prof',
  templateUrl: './admin-prof.component.html',
  styleUrls: ['./admin-prof.component.css']
})
export class AdminProfComponent implements OnInit {
  data=new Data3();
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.data=this.authan.staffOrAdminDetails;
  }


}
class Data3{
  firstName:string;
  lastName:string;
  age:number;
  email:string;
  gender:number;
  contact:number;
  campuses: string [];

}
