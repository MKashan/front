import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-teacher-prof',
  templateUrl: './teacher-prof.component.html',
  styleUrls: ['./teacher-prof.component.css']
})
export class TeacherProfComponent implements OnInit {
  data=new Data2();
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.data=this.authan.teacherDetails;
  }

}
class Data2{
  firstName:string;
  lastName:string;
  age:number;
  email:string;
  gender:number;
  contact:number;
  campuses: string [];
  courses: string [];
}
