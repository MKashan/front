import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentProfComponent } from './student-prof.component';

describe('StudentProfComponent', () => {
  let component: StudentProfComponent;
  let fixture: ComponentFixture<StudentProfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentProfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentProfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
