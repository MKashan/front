import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-view-student',
  templateUrl: './view-student.component.html',
  styleUrls: ['./view-student.component.css']
})
export class ViewStudentComponent implements OnInit {

  value: string;
  responce= {};
  Data= [];
  courses=[];
  campus=[];
  toggle= false;
  message: string;
  name:string;
  CampusName:string;
  student=[];

  constructor(private authan: GlobalServService) {


  }

  ngOnInit() {
    this.authan.getStudents().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              console.log('sdsdsdsa',responce.data)
               this.value=responce.data[0].id;
              this.student= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.value= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  show() {
    console.log('value',this.value)
    this.authan.getStudent(this.value).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('ssssss',responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.Data = responce.data;
              this.message= responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

}
