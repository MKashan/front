import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-view-admin',
  templateUrl: './view-admin.component.html',
  styleUrls: ['./view-admin.component.css']
})
export class ViewAdminComponent implements OnInit {

  value: string;
  responce= {};
  Data= {};
  toggle= false;
  message: string;
  admin=[];
  constructor( private authan: GlobalServService) {


  }
  ngOnInit() {
    this.toggle = false;
    this.authan.getAdmins().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.value=responce.data[0].id;
              this.admin= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.value= deviceValue.target.value;

  }
  toggles(){
    this.toggle= false;
  }
  show() {
    this.authan.getAdmin(this.value).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              console.log(responce.data)
              this.toggle = true;
              this.Data= responce.data[0];
              this.message= responce.message
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }


}
