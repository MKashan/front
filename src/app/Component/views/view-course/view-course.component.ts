import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-view-course',
  templateUrl: './view-course.component.html',
  styleUrls: ['./view-course.component.css']
})
export class ViewCourseComponent implements OnInit {
  CourseName: string;
  CourseClass: string;
  responce= {};
  Datas= [];
  section = []
  toggle= false;
  message: string;
  course=[];
  constructor(private authan: GlobalServService) {


  }
  toggles() {
    this.toggle = false;
  }
  ngOnInit() {
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {

          switch ( responce.status) {
            case 200:
              this.CourseName=responce.data[0].name;
              this.CourseClass=responce.data[0].class;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.CourseName= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.CourseClass= deviceValue.target.value;

  }
  show() {

    this.authan.getCourse(this.CourseName, this.CourseClass).subscribe(
      responce => {
        console.log('osma', responce.data)
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.Datas = responce.data;
              console.log('abhisdasdas',responce.data[0].teachers)
              this.message= responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

}
