import {Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
import {Http} from '@angular/http';
import {document} from '@angular/platform-browser/src/facade/browser';

@Component({
  selector: 'app-vcampus',
  templateUrl: './vcampus.component.html',
  styleUrls: ['./vcampus.component.css']
})
export class VCampusComponent implements OnInit {
  value: string;
  responce= {};
  Datas= [];
  toggle= false;
  message: string;
  campus=[];
  temp:string;
  constructor(private authan: GlobalServService) {
  }

  ngOnInit() {
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.value=responce.data[0].name;
              this.campus= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.value= deviceValue.target.value;

  }
  toggles(){
    this.toggle= false;
  }
  show() {
    console.log('sdsadsadsadad',this.value)
    this.authan.getCampus(this.value).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.data);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.Datas = responce.data;
              this.message= responce.message;
              break;
            case 404:
              this.toggle = true;
              this.Datas = responce.data;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.Datas = responce.data;
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }

}
