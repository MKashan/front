import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-course-add',
  templateUrl: './course-add.component.html',
  styleUrls: ['./course-add.component.css']
})
export class CourseAddComponent implements OnInit {
  type: string;
  data = new Data();
  toggle = false;
  message: string;

  constructor(private  authan: GlobalServService) {
  }

  ngOnInit() {

  }
  toggles(){
    this.toggle= false;
  }
AddCourse(){
    this.data.crTeacherId=0;
  this.authan.addCourse(this.data).subscribe(
    responce => {
      console.log(responce);
      if ( responce.status != null) {
        console.log(responce.status);
        switch ( responce.status) {
          case 200:
            this.toggle = true;
            this.message = responce.message;

            break;
          case 403:
            this.toggle = true;
            this.message = responce.message;
            break;
          case 404:
            this.toggle = true;
            this.message = responce.message;
            break;
          case 500:
            this.toggle = true;
            this.message = responce.message;
            break;


        }


      }
    }
  );
}



}

class Data{
  name:string;
  syllabus:string;
  class:number;
  contact:number;
  crTeacherId:number;

}
